#ifndef DISK_H
#define DISK_H

#include <QObject>
#include <file.h>
#include <backupdisk.h>

class Disk : public QObject
{
    Q_OBJECT
public:
    explicit Disk(QObject *parent = 0);
    int id;
    QString name;
    QString hash;
    QString path;
    QList<BackupDisk> backup_disks;
    QList<File> files;
signals:

public slots:
};

#endif // DISK_H
