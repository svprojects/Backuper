#ifndef DATABASE_H
#define DATABASE_H

#include <QObject>
#include <QtSql>

#define DATABASE_HOSTNAME   "./"
#define DATABASE_NAME       "lims_server"
#define DATABASE_USER       "root"
#define DATABASE_PASSWORD   "484965"

class DataBase : public QObject
{
    Q_OBJECT
public:
    explicit DataBase(QObject *parent = 0);
    ~DataBase();

    void connectToDataBase();
    QSqlDatabase getDataBase();
    bool isOpen();
    void closeDataBase();
private:
    static QSqlDatabase db;
    QString connectionDBName;
    Logger *logger;
private:
    bool openDataBase();
};

#endif // DATABASE_H
