#ifndef BACKUPDISK_H
#define BACKUPDISK_H

#include <QObject>

class BackupDisk : public QObject
{
    Q_OBJECT
public:
    explicit BackupDisk(QObject *parent = 0);

    int id;
    QString name;
    bool isActual;
};

#endif // BACKUPDISK_H
