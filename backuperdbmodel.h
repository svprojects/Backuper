#ifndef BACKUPERDBMODEL_H
#define BACKUPERDBMODEL_H

#include <QObject>
#include <QtSql>
#include <disk.h>
#include <database.h>

class BackuperDBModel : public QObject
{
    Q_OBJECT
public:
    explicit BackuperDBModel(QObject *parent = 0);
public slots:
    bool addDisk(QString name, QString hash);
    void deleteDisk();
    void addBackupsDisk();
    QList<BackupDisk*> getBackupDisksByDisk(int id_disk);
    int getNotBackupCount(int id_disk);
    bool checkActualDisk(int id_disk, int id_backup_disk);
    bool checkActualFile(int id_file, int id_backup_disk);

private:
    Disk disk;
    DataBase *db;
};

#endif // BACKUPERDBMODEL_H
