﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtWidgets/QtWidgets>
#include <QtCore/QtCore>

#include <filemodel.h>
#include <filemanager.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
public slots:
    void openDiskForBackup();
    void addBackupDisk();
    void backup();

private:
    Ui::MainWindow *ui;
    FileManager *fileManager;
    FileModel* model;
    QStringListModel *backupListModel;
    QString diskPath;
    QString backupPath;
};

#endif // MAINWINDOW_H
