﻿#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
//    FileModel *model = new FileModel;
    model = new FileModel;
    fileManager = new FileManager(this);
    backupListModel = new QStringListModel(fileManager->getAllBackupDisk(true),this);
    QStringListModel *allListModel = new QStringListModel(fileManager->getAllBackupDisk(false),this);
    model->setRootPath(QDir::rootPath());

    ui->treeView->setModel(model);
    ui->treeView->setRootIndex(model->index(QDir::rootPath()));
    ui->treeView->header()->setSectionResizeMode(QHeaderView::Stretch);
    ui->backupDiskPath->setModel(backupListModel);
    ui->CBAllDisk->setModel(allListModel);

    connect(ui->actionOpen_disk,SIGNAL(triggered(bool)),this,SLOT(openDiskForBackup()));
    connect(ui->addBuckupDisk,SIGNAL(clicked(bool)),this,SLOT(addBackupDisk()));
    connect(ui->Backup,SIGNAL(clicked(bool)),this,SLOT(backup()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::openDiskForBackup()
{
    QString filePath = "";
//    if (ui->diskPath->text()!="") filePath = ui->diskPath->text();
//    else filePath = QDir::homePath();
    filePath = QString("C://");
    diskPath= QFileDialog::getExistingDirectory(0, "Open", filePath);
    ui->diskPath->setText(diskPath);
    ui->treeView->setRootIndex(model->index(diskPath));
//    QStringList filters;
//    filters << ".backup";
//    model->setNameFilters(filters);

}
void MainWindow::addBackupDisk()
{
    QString filePath = ui->CBAllDisk->currentText();
//    QString addsDiskPath= QFileDialog::getExistingDirectory(0, "Открыть", filePath);
//    if(ui->backupDiskPath->model()->insertRow(ui->backupDiskPath->model()->rowCount())) {
//        QModelIndex index = ui->backupDiskPath->model()->index(ui->backupDiskPath->model()->rowCount() - 1, 0);
//        ui->backupDiskPath->model()->setData(index, addsDiskPath);
//    }
    fileManager->addBackupDisk(filePath);
    backupListModel->setStringList(fileManager->getAllBackupDisk(true));
}
void MainWindow::backup()
{
    backupPath = ui->backupDiskPath->currentText();
//    fileManager->setBackup(diskPath, diskPath, backupPath);
    fileManager->setBackup(diskPath, diskPath, "C://backup/");
}
