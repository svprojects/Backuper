#include "database.h"

DataBase::DataBase(QObject *parent) : QObject(parent)
{
}
DataBase::~DataBase()
{
    closeDataBase();
}
void DataBase::connectToDataBase()
{
    this->openDataBase();
}
QSqlDatabase DataBase::getDataBase()
{
    return db;
}
bool DataBase::isOpen()
{
    return db.isOpen();
}
bool DataBase::openDataBase()
{
    if(!db.isOpen())
    {
        db =  QSqlDatabase::addDatabase("QSQLITE","BackuperDBConnection");
        db.setHostName(DATABASE_HOSTNAME);
        db.setDatabaseName(DATABASE_NAME);
        db.setUserName(DATABASE_USER);
        db.setPassword(DATABASE_PASSWORD);

        if(db.open())
            return true;
    }
    return false;
}
void DataBase::closeDataBase()
{
    db.close();
}
