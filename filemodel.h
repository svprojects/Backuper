﻿#ifndef FILEMODEL_H
#define FILEMODEL_H

#include <QObject>
#include <QtWidgets/QFileSystemModel>
#include <QtGui/QtGui>
#include <filemanager.h>

enum Column {
    NAME = 0,
    SIZEFILE = 1,
    TYPE = 2,
    DATAMODIFER = 3,
    FLAG = 4,
    LAST
};
class FileModel : public QFileSystemModel
{
public:
    FileModel();

private:
    int columnCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    QVariant headerData( int section, Qt::Orientation orientation, int role ) const;
    bool setData(const QModelIndex &index, const QVariant &value, int role);
    //чтение файла info и возврат список файлов с бекапом и их хэшем

};

#endif // FILEMODEL_H
