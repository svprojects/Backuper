#include "backuperdbmodel.h"

BackuperDBModel::BackuperDBModel(QObject *parent) : QObject(parent)
{
    db = new DataBase(this);
    db->connectToDataBase();
}

bool BackuperDBModel::addDisk(QString name, QString hash)
{
    if(db->isOpen())
    {
        QSqlQuery query(db->getDataBase());
        QString queryString = QString("INSERT INTO disks (name, hash) values ('%1', '%2');")
                .arg(name, hash);
        query.exec(queryString);
        if(!(query.lastError().type() == QSqlError::NoError))
            return false;
    }
    return true;
}
bool BackuperDBModel::deleteDisk(int id)
{
    if(db->isOpen())
    {
        QSqlQuery query(db->getDataBase());
        QString deleteString = QString("DELETE FROM disks WHERE id = %1")
                .arg(TABLENAME_QUASAR_HISTEST_UPDATE, QString::number(id));
        query.exec(deleteString);
        if(!(query.lastError().type() == QSqlError::NoError))
            return false;
    }
    return true;
}
bool BackuperDBModel::addBackupsDisk(QString name)
{
    if(db->isOpen())
    {
        QSqlQuery query(db->getDataBase());
        QString queryString = QString("INSERT INTO backup_disk (name) values ('%1');")
                .arg(name);
        query.exec(queryString);
        if(!(query.lastError().type() == QSqlError::NoError))
            return false;
    }
    return true;
}
QList<BackupDisk*> BackuperDBModel::getBackupDisksByDisk(int id_disk)
{
    QList<BackupDisk> result;
    if(db->isOpen())
    {
        QSqlQuery query(db->getDataBase());
        QString queryString = QString("SELECT * FROM backup_disk WHERE id IN (SELECT id_backup_disk FROM disk_via_backup WHERE id_disk='%1');")
                .arg(id_disk);
        query.exec(queryString);
        if(query.lastError().type() == QSqlError::NoError)
        {
            while(query.next())
            {
                BackupDisk *backupDisk = new BackupDisk();
                backupDisk->id = query.value("id").toInt();
                backupDisk->name = query.value("name").toString();
                backupDisk->isActual = query.value("actual").toBool();
                result.append(backupDisk);
            }
        }
    }
    return result;
}
int BackuperDBModel::getNotBackupCount(int id_disk)
{
    if(db->isOpen())
    {
        QSqlQuery query(db->getDataBase());
        QString queryString = QString("SELECT COUNT(*) FROM files WHERE id_disk='%1' AND id IN (SELECT id_files FROM files_has_backup);")
                .arg(id_disk);
        query.exec(queryString);
        if(!(query.lastError().type() == QSqlError::NoError))
        {
            query.first();
            int count = query.value(0).toInt();
            return count;
        }
        return -1;
    }
    return -1;
}
bool BackuperDBModel::checkActualDisk(int id_disk, int id_backup_disk)
{
    if(db->isOpen())
    {
        QSqlQuery query(db->getDataBase());
        QString queryString = QString("SELECT id FROM files WHERE id_disk='%1'")
                .arg(id_disk);
        query.exec(queryString);
        if(query.lastError().type() == QSqlError::NoError)
        {
            while(query.next())
            {
                int id_file = query.value("id").toInt();
                if(!checkActualFile(id_file, id_backup_disk))
                        return false;
            }
        }
    }
    return true;
}
bool BackuperDBModel::checkActualFile(int id_file, int id_backup_disk)
{
    if(db->isOpen())
    {
        QSqlQuery query(db->getDataBase());
        QString queryString = QString("SELECT COUNT(*) FROM files AS t1, backup_files AS t2 WHERE t1.id='%1'").arg(id_file)
                .append(" AND t2.id_backup_disk='%2' AND t1.hash!=t2.hash ")
                .arg(id_backup_disk)
                .append("AND t1.id IN (SELECT id_files FROM files_has_backup AS t3  WHERE t2.id=t3.id_backup_files);");
        query.exec(queryString);
        if(!(query.lastError().type() == QSqlError::NoError))
        {
            query.first();
            int count = query.value(0).toInt();
            if (count != 0) return true;
        }
    }
    return false;
}
