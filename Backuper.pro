#-------------------------------------------------
#
# Project created by QtCreator 2016-08-08T17:38:56
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Backuper
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
        filemodel.cpp \
    filemanager.cpp \
    disk.cpp \
    file.cpp \
    backuperdbmodel.cpp \
    backupdisk.cpp \
    database.cpp

HEADERS  += mainwindow.h \
        filemodel.h \
    filemanager.h \
    disk.h \
    file.h \
    backuperdbmodel.h \
    backupdisk.h \
    database.h

FORMS    += mainwindow.ui
