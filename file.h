#ifndef FILE_H
#define FILE_H

#include <QObject>

class File : public QObject
{
    Q_OBJECT
public:
    explicit File(QObject *parent = 0);
    int id;
    QString name;
    QString path;
    QString hash;
    QVector<int> backup_files;
signals:

public slots:
};

#endif // FILE_H
