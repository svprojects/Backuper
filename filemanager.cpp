﻿#include "filemanager.h"

FileManager::FileManager(QObject *parent) : QObject(parent)
{
}
void FileManager::setPath(QString disk, QString backup)
{
    diskPath = disk;
    backupPath = backup;
}
bool FileManager::addBackupDisk(QString path)
{
    QString bPath = path+"/.backup";
    QDir dir (bPath);
    dir.mkpath(bPath);
    QFile backupInfoFile(bPath+"/info");
    backupInfoFile.open(QIODevice::WriteOnly);
    backupInfoFile.close();
    return true;
}
QStringList FileManager::getAllBackupDisk(bool isFilter)
{
    QFileInfoList disks = QDir::drives();
    QStringList list;
    for(int i=0;i<disks.size();++i)
    {
        if(isFilter)
        {
            if(QFile(disks[i].filePath()+"/.backup").exists())
            {
                list.append(disks[i].filePath());
            }
        }
        else
            list.append(disks[i].filePath());
    }
    return list;
}
void FileManager::setInfoFile(QFileInfo fileInfo, QString backupPath)
{
    QString path = fileInfo.absoluteFilePath();
    QDir dir(backupPath);
    dir.mkpath(backupPath+"/.backup/");
    QFile backupInfoFile(backupPath+"./.backup/info");
    QTextStream out(&backupInfoFile);
    if(backupInfoFile.open(QIODevice::Append))
    {
        QCryptographicHash hash(QCryptographicHash::Md5);
        QFile in(path);
        if (in.open(QIODevice::ReadOnly))
        {
            char buf[2048];
            while (in.read(buf, 2048) > 0)
            {
                hash.addData(buf, 2048);
            }
            in.close();
        }
        out<<QString("%1:%2\n").arg(fileInfo.fileName(),QString(hash.result().toHex()));
    }
    backupInfoFile.close();
}

void FileManager::setBackupInfo(QString path, QString backupDiskFiles, QString prefix)
{
    QFileInfo fileInfo(path);
    QString absFP = fileInfo.absolutePath();
    QString relativePath = absFP.mid(prefix.size());
    QDir backupDir(backupDiskFiles);
    backupDir.mkpath(backupDiskFiles+relativePath);

    if(QFile::copy(path,QString("%1%2/%3").arg(backupDiskFiles,relativePath,fileInfo.fileName())))
    {
        setInfoFile(fileInfo, backupDiskFiles+relativePath);
        setInfoFile(fileInfo, fileInfo.absolutePath());
    }
}
bool FileManager::isBackupNeed(QFile oringinFilePath, QString backupFilePath)
{

}
QHash<QString,QString> FileManager::getSavedList(QString filePath)
{
    QHash<QString,QString> hashFile;
    QFile file(filePath);
    QTextStream in(&file);
    if(file.open(QIODevice::ReadOnly))
    {
        QTextStream out(&file);
        while(!out.atEnd())
        {
            QString line = out.readLine();
            hashFile.insert(line.split(':')[0],line.split(':')[1]);
        }
    }
    file.close();
    return hashFile;
}
bool FileManager::isSaved(QFileInfo fileInfo)
{
    QFile file(fileInfo.path()+"/.backup/info");
    return file.exists();
}

QString FileManager::getHashsumByFile(QFileInfo fileInfo)
{
    if(isSaved(fileInfo))
    {
        QHash<QString,QString> hashFile = getSavedList(fileInfo.path()+"/.backup/info");
        if(hashFile.keys().contains(fileInfo.fileName()))
        {
            return hashFile[fileInfo.fileName()];
        }
    }
    return "";
}
bool FileManager::setBackup(QString prefix, QString path, QString backupDisk)
{
    QString backupDiskFiles = backupDisk+"//files/";
    QFileInfo fileInfo(path);
    if(fileInfo.isDir())
    {
        if(fileInfo.fileName()==".backup")
        {
            return true;
        }
        QDir dir(path);
        QFileInfoList list = dir.entryInfoList();
        bool isAllBackup = true;
        for(int i=0;i<list.size();i++)
        {
            if((list.at(i).fileName()!=".")&&(list.at(i).fileName()!=".."))
            {
                isAllBackup = isAllBackup*setBackup(prefix, list.at(i).absoluteFilePath(),backupDisk);
            }
        }
        if(isAllBackup)
        {
            QFileInfo fileInfo(path);
            QString absFP = fileInfo.absolutePath();
            QString relativePath = absFP.mid(prefix.size());
            QDir backupDir(backupDiskFiles);
            backupDir.mkpath(backupDiskFiles+relativePath);

            setInfoFile(fileInfo, backupDiskFiles+relativePath);
            setInfoFile(fileInfo, fileInfo.absolutePath());
        }
    }
    else
    {
        setBackupInfo(path, backupDiskFiles, prefix);
    }
    return true;
}
