﻿#ifndef FILEMANAGER_H
#define FILEMANAGER_H

#include <QObject>
#include <QtCore/QtCore>
#include <backuperdbmodel.h>

class FileManager : public QObject
{
    Q_OBJECT
public:
    explicit FileManager(QObject *parent = 0);
    void setPath(QString disk, QString backup);
    void setInfoFile(QFileInfo fileInfo, QString backupPath);
    void setBackupInfo(QString path, QString backupDiskFiles, QString prefix);

    static QHash<QString,QString> getSavedList(QString filePath);
    static QString getHashsumByFile(QFileInfo fileInfo);
    static bool isSaved(QFileInfo fileInfo);
signals:

public slots:
    bool addBackupDisk(QString path);
    QStringList getAllBackupDisk(bool isFilter);
    bool setBackup(QString prefix, QString path, QString backupDisk);
    bool isBackupNeed(QFile oringinFilePath, QString backupFilePath);
private:
    QString diskPath;
    QString backupPath;
    BackuperDBModel *DBModel;
};

#endif // FILEMANAGER_H
