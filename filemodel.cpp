﻿#include "filemodel.h"

FileModel::FileModel()
{
}
int FileModel::columnCount(const QModelIndex &parent) const
{
    return LAST;
}
QVariant FileModel::data(const QModelIndex &index, int role) const
{
    if(index.isValid())
    {
        if(role == Qt::DisplayRole || role == Qt::EditRole)
        {
            switch (index.column())
            {
            case FLAG:
                QFileInfo file = QFileSystemModel::fileInfo(index);
                QString rr = FileManager::getHashsumByFile(file);
                return rr;
            }
        }
        if(role==Qt::BackgroundColorRole)
        {
            QModelIndex indexFlag = this->index(QFileSystemModel::filePath(index),FLAG);
            int row = index.row();
            int column = index.column();
            QString flag = indexFlag.data().toString();
            if(flag!="")
                return QColor(Qt::darkGreen);
            else
                return QColor(Qt::red);
        }
    }
    return QFileSystemModel::data(index, role);
}
bool FileModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    return QFileSystemModel::setData(index, value, role);
}
QVariant FileModel::headerData( int section, Qt::Orientation orientation, int role ) const
{
    if( role == Qt::DisplayRole )
    {
        switch( section ) {
        case NAME:
            return QFileSystemModel::headerData(section,orientation, role);
        case SIZEFILE:
            return QFileSystemModel::headerData(section,orientation, role);
        case TYPE:
            return QFileSystemModel::headerData(section,orientation, role);
        case DATAMODIFER:
            return QFileSystemModel::headerData(section,orientation, role);
        case FLAG:
            return "FLAG";
        }
    }
    return QFileSystemModel::headerData(section,orientation, role);
}

